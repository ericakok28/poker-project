package model;

/**
 * @purpose
 * 
 * Class with parameters requiring a specific card suit, card type, and image to
 * create the card.
 *
 * 
 * @author Luisa Molina
 * @author Erica Kok
 * 
 * @dueDate 3/21/16
 */

import java.awt.Image;

public class Card 
{
	private CardSuit mySuit;
	private CardType myType;
	private Image myImage;
	private boolean myIsSelected;
	private boolean myIsFaceUp;
	
	/**
	 * Constructor, creates a new Card with a suit, type, and image.
	 * 
	 * @param mySuit, myType, myImage
	 * 
	 * @author Erica Kok
	 * @author Luisa Molina
	 */

	public Card(CardSuit suit, CardType type, Image image) 
	{
		mySuit = suit;
		myType = type;
		myImage = image;
		myIsSelected = false;
		myIsFaceUp = false;
	}

	/**
	 * Method to flip the card to the opposite face.
	 * 
	 * 
	 * @author Erica Kok
	 * @author Luisa Molina
	 */
	public void flip() 
	{
		myIsFaceUp = !myIsFaceUp;
	}

	/**
	 * Method to determine whether or not the side of the card
	 * with values is facing up or down.
	 * 
	 * @return returns a boolean, true if it's face up or not.
	 * 
	 * @author Erica Kok
	 * @author Luisa Molina
	 */
	
	public boolean isFaceUp() 
	{
		return myIsFaceUp;
	}

	/**
	 * Method used to determine whether or not a card has been selected.
	 * 
	 * @return true if a card has been selected, otherwise return false.
	 * 
	 * @author Erica Kok
	 * @author Luisa Molina
	 */
	
	public boolean isSelected() 
	{
		return myIsSelected;
	}

	public void toggleSelected() 
	{
		myIsSelected = !myIsSelected;
	}

	/**
	 * Method to compare the current card to another
	 * 
	 * @param card (The card that the main one is being compared to)
	 * 
	 * @return A value of 1, 0, -1 showing whether or not the card
	 * is less than, equal to, or greater than the main card.
	 * 
	 * @author Erica Kok
	 * @author Luisa Molina
	 */
	
	public int compareTo(Card card) 
	{
		if(myType.getType() > card.getType().getType())
		{
			return 1;
		}
		else if (myType.getType() == card.getType().getType())
		{
			return 0;
		}
		else
		{
			return -1;
		}
	}
	
	/**
	 * Method to create a clone of a Card.
	 * 
	 * @return the cloned Card
	 * 
	 * @author Erica Kok
	 * @author Luisa Molina
	 */

	public Object clone() 
	{
		Object myClone = new Card(mySuit, myType, myImage);
		return myClone;
	}
	
	/**
	 * Method to print the Card as a string.
	 * 
	 * @return string with the Card.
	 * 
	 * @author Erica Kok
	 * @author Luisa Molina
	 */

	public String toString() 
	{
		return "Card: Type " + myType + ", Suit " + mySuit + ", Image" + myImage;
	}
	
	/**
	 * Method to get the suit of the Card.
	 * 
	 * @return suit of the Card.
	 * 
	 * @author Erica Kok
	 * @author Luisa Molina
	 */

	public CardSuit getSuit()
	{
		return mySuit;
	}
	
	/**
	 * Method to get the type of the Card.
	 * 
	 * @return type of the Card.
	 * 
	 * @author Erica Kok
	 * @author Luisa Molina
	 */

	public CardType getType()
	{
		return myType;
	}
	
	/**
	 * Method to get the image of the Card.
	 * 
	 * @return image of the Card.
	 * 
	 * @author Erica Kok
	 * @author Luisa Molina
	 */

	public Image getImage()
	{
		return myImage;
	}
}