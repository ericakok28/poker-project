package model;

/**
 * @purpose
 * 
 * Class used to find the type of hand a player has within a poker game.
 * The input requires an integer of the number of cards within the hand
 * to be evaluated.
 * 
 * @author Erica Kok
 * @author Luisa Molina
 * 
 * @dueDaate 3/21/16
 */

public class PokerHand extends Hand
{
	private int myNumberCards;
	private int myMaxNumberCards;
	private PokerHandRanking myRanking;
	private Hand myPokerHand;
	
	/**
	 * Constructor that creates a new PokerHand with a max number of 5 Cards in the Hand.
	 * 
	 * @param max number of Cards
	 * 
	 * @author Luisa Molina
	 * @author Erica Kok
	 */

	public PokerHand(int maxNum)
	{
		super(maxNum);
		myMaxNumberCards = maxNum;
	}
	
	/**
	 * Method to determine the ranking of a PokerHand.
	 * 
	 * @return the ranking of the Hand
	 * 
	 * @author Luisa Molina
	 * @author Erica Kok
	 */

	public PokerHandRanking determineRanking()
	{
		if(isRoyalFlush() == true)
		{
			return myRanking;
		}
		else if(isStraightFlush() == true)
		{
			return myRanking;
		}
		else if(isFourOfKind() == true)
		{
			return myRanking;
		}
		else if(isFullHouse() == true)
		{
			return myRanking;
		}
		else if(isFlush() == true)
		{
			return myRanking;
		}
		else if(isStraight() == true)
		{
			return myRanking;
		}
		else if(isThreeOfKind() == true)
		{
			return myRanking;
		}
		else if(isTwoPair() == true)
		{
			return myRanking;
		}
		else if(isPair() == true)
		{
			return myRanking;
		}
		else if(isHighCard() == true)
		{
			return myRanking;
		}
		return myRanking;
	}
	
	
	/**
	 * Method to compare the 2 Players' PokerHands.
	 * 
	 * @param pokerHand
	 * 
	 * @return 1 if myPlayer[1] has a higher PokerHand, otherwise return 0
	 * 
	 * @author Luisa Molina
	 * @author Erica Kok
	 */
	
	public int compareTo(PokerHand pokerHand)
	{
		if (this.getRanking() > pokerHand.getRanking())
		{
			return 1;
		}
		else if (this.getRanking() < pokerHand.getRanking())
		{
			return -1;
		}
		else
		{
			return 0;
		}
	}
	
	/**
	 * Method to print the PokerHand as a string.
	 * 
	 * @return the PokerHand as a string
	 * 
	 * @author Luisa Molina
	 * @author Erica Kok
	 */

	public String toString()
	{
		return "Hand: " + myPokerHand;
	}
	
	/**
	 * Method to get the ranking of a PokerHand.
	 * 
	 * @return the ranking of a PokerHand
	 * 
	 * @author Luisa Molina
	 * @author Erica Kok
	 */

	public int getRanking()
	{
		return myRanking.getRank();
	}
	
	/**
	 * Method to get the number of Cards in a Hand.
	 * 
	 * @return the number of Cards in a Hand
	 * 
	 * @author Luisa Molina
	 * @author Erica Kok
	 */

	public int getNumberCards()
	{
		return myNumberCards;
	}
	
	/**
	 * Method to get the max number of Cards in a Hand.
	 * 
	 * @return the max number of Cards in the Hand
	 * 
	 * @author Luisa Molina
	 * @author Erica Kok
	 */

	public int getMaxNumberCards()
	{
		return myMaxNumberCards;
	}
	
	/**
	 * Method to check to see if the PokerHand is a High Card.
	 * 
	 * @return true if the PokerHand is a High Card, otherwise return false
	 * 
	 * @author Luisa Molina
	 * @author Erica Kok
	 */

	public boolean isHighCard()
	{
		if(!isPair() && !isTwoPair() && !isThreeOfKind() && !isStraight() && !isFlush() && !isFullHouse() && !isFourOfKind() && !isStraightFlush() && !isRoyalFlush())
		{
			myRanking = PokerHandRanking.HIGH_CARD;
			return true;
		}
		else
		{
			return false;
		}
	}
	
	/**
	 * Method to check to see if the PokerHand is a Pair.
	 * 
	 * @return true if the PokerHand is a Pair, otherwise return false
	 * 
	 * @author Luisa Molina
	 * @author Erica Kok
	 */

	public boolean isPair()
	{
		/*
		 * Checks to see if there is 1 pair of Cards whose types are the same in different
		 * positions in the Hand. Set myRanking to PAIR.
		 */
		
		if (myHand.get(0).getType().ordinal() == myHand.get(1).getType().ordinal() 
				&& myHand.get(1).getType().ordinal() != myHand.get(2).getType().ordinal() 
				&& myHand.get(2).getType().ordinal() != myHand.get(3).getType().ordinal() 
				&& myHand.get(3).getType().ordinal() != myHand.get(4).getType().ordinal())
		{
			myRanking = PokerHandRanking.PAIR;
			return true;
		}
		else if (myHand.get(1).getType().ordinal() == myHand.get(2).getType().ordinal() 
				&& myHand.get(0).getType().ordinal() != myHand.get(1).getType().ordinal() 
				&& myHand.get(2).getType().ordinal() != myHand.get(3).getType().ordinal() 
				&& myHand.get(3).getType().ordinal() != myHand.get(4).getType().ordinal())
		{
			myRanking = PokerHandRanking.PAIR;
			return true;
		}
		else if (myHand.get(2).getType().ordinal() == myHand.get(3).getType().ordinal() 
				&& myHand.get(0).getType().ordinal() != myHand.get(1).getType().ordinal() 
				&& myHand.get(1).getType().ordinal() != myHand.get(2).getType().ordinal() 
				&& myHand.get(3).getType().ordinal() != myHand.get(4).getType().ordinal())
		{
			myRanking = PokerHandRanking.PAIR;
			return true;
		}
		else if (myHand.get(3).getType().ordinal() == myHand.get(4).getType().ordinal() 
				&& myHand.get(0).getType().ordinal() != myHand.get(1).getType().ordinal() 
				&& myHand.get(1).getType().ordinal() != myHand.get(2).getType().ordinal() 
				&& myHand.get(2).getType().ordinal() != myHand.get(3).getType().ordinal())
		{
			myRanking = PokerHandRanking.PAIR;
			return true;
		}
		else
		{
			return false;
		}
	}
	
	/**
	 * Method to check to see if the PokerHand is a Two Pair.
	 * 
	 * @return true if the PokerHand is a Two Pair, otherwise return false
	 * 
	 * @author Luisa Molina
	 * @author Erica Kok
	 */

	public boolean isTwoPair()
	{
		/*
		 * Checks to see if there are 2 pairs Cards whose types are the same in different
		 * positions in the Hand. Set myRanking to TWO_PAIR.
		 */
		
		if (myHand.get(0).getType().ordinal() == myHand.get(1).getType().ordinal() 
				&& myHand.get(2).getType().ordinal() == myHand.get(3).getType().ordinal() 
				&& myHand.get(1).getType().ordinal() != myHand.get(2).getType().ordinal() 
				&& myHand.get(3).getType().ordinal() != myHand.get(4).getType().ordinal()
				&& myHand.get(1).getType().ordinal() != myHand.get(4).getType().ordinal())
		{
			myRanking = PokerHandRanking.TWO_PAIR;
			return true;
		}
		else if (myHand.get(0).getType().ordinal() == myHand.get(1).getType().ordinal() 
				&& myHand.get(3).getType().ordinal() == myHand.get(4).getType().ordinal() 
				&& myHand.get(1).getType().ordinal() != myHand.get(2).getType().ordinal() 
				&& myHand.get(3).getType().ordinal() != myHand.get(2).getType().ordinal()
				&& myHand.get(1).getType().ordinal() != myHand.get(4).getType().ordinal())
		{
			myRanking = PokerHandRanking.TWO_PAIR;
			return true;
		}
		else if (myHand.get(1).getType().ordinal() == myHand.get(2).getType().ordinal() 
				&& myHand.get(3).getType().ordinal() == myHand.get(4).getType().ordinal() 
				&& myHand.get(0).getType().ordinal() != myHand.get(1).getType().ordinal() 
				&& myHand.get(0).getType().ordinal() != myHand.get(3).getType().ordinal()
				&& myHand.get(2).getType().ordinal() != myHand.get(3).getType().ordinal())
		{
			myRanking = PokerHandRanking.TWO_PAIR;
			return true;
		}
		else
		{
			return false;
		}
	}
	
	/**
	 * Method to check to see if the PokerHand is a Three of Kind.
	 * 
	 * @return true if the PokerHand is a Three of Kind, otherwise return false
	 * 
	 * @author Luisa Molina
	 * @author Erica Kok
	 */

	public boolean isThreeOfKind()
	{
		/*
		 * Checks to see if there are 3 Cards whose types are the same in different
		 * positions in the Hand. Set myRanking to THREE_OF_KIND.
		 */
		
		if (myHand.get(0).getType().ordinal() == myHand.get(1).getType().ordinal() 
				&& myHand.get(1).getType().ordinal() == myHand.get(2).getType().ordinal() 
				&& myHand.get(2).getType().ordinal() != myHand.get(3).getType().ordinal() 
				&& myHand.get(3).getType().ordinal() != myHand.get(4).getType().ordinal())
		{
			myRanking = PokerHandRanking.THREE_OF_KIND;
			return true;
		}
		else if (myHand.get(2).getType().ordinal() == myHand.get(3).getType().ordinal() 
				&& myHand.get(3).getType().ordinal() == myHand.get(4).getType().ordinal() 
				&& myHand.get(2).getType().ordinal() != myHand.get(1).getType().ordinal() 
				&& myHand.get(1).getType().ordinal() != myHand.get(0).getType().ordinal())
		{
			myRanking = PokerHandRanking.THREE_OF_KIND;
			return true;
		}
		else 
		{
			return false;
		}
	}
	
	/**
	 * Method to check to see if the PokerHand is a Straight.
	 * 
	 * @return true if the PokerHand is a Straight, otherwise return false
	 * 
	 * @author Luisa Molina
	 * @author Erica Kok
	 */

	public boolean isStraight()
	{
		/*
		 * Checks to see if the types of the Cards are in ascending order.
		 */
		
		boolean ascending = true;
		for (int i = 0; i < myHand.size()-1; i++)
		{
			if (myHand.get(i).getType().getType()+1 == myHand.get(i+1).getType().getType())
			{
				ascending = ascending && true;
			}
			else
			{
				ascending = false;

			}
		}
		
		/*
		 * If yes, then set myRanking to STRAIGHT.
		 */
		
		if (ascending == true)
		{
			myRanking = PokerHandRanking.STRAIGHT;
			return true;
		}
		else
		{
			return false;
		}
	}
	
	/**
	 * Method to check to see if the PokerHand is a Flush.
	 * 
	 * @return true if the PokerHand is a Flush, otherwise return false
	 * 
	 * @author Luisa Molina
	 * @author Erica Kok
	 */

	public boolean isFlush()
	{
		/*
		 * Checks to see if the suits of the Cards are the same.
		 */
		
		boolean matchSuits = true;
		for (int i = 0; i < myHand.size() - 1; i++)
		{
			if (myHand.get(i).getSuit().getSuit().equals(myHand.get(i + 1).getSuit().getSuit()))
			{
				matchSuits = matchSuits && true;
			} else
			{
				matchSuits = false;
				break;
			}
		}
		
		/*
		 * If yes, then set myRanking to Flush and return true.
		 */

		if (matchSuits == true)
		{
			myRanking = PokerHandRanking.FLUSH;
			return true;
		}
		else
		{
			return false;
		}
	}
	
	/**
	 * Method to check to see if the PokerHand is a Full House.
	 * 
	 * @return true if the PokerHand is a Full House, otherwise return false
	 * 
	 * @author Luisa Molina
	 * @author Erica Kok
	 */

	public boolean isFullHouse()
	{	
		/*
		 * Checks to see if the types of 2 Cards are the same, and the types of 3 different Cards are the same.
		 */
		
		boolean check = myHand.get(0).getType() == (myHand.get(1).getType());
		if (check && myHand.get(2).getType() != myHand.get(0).getType() 
				&& myHand.get(2).getType() == myHand.get(3).getType() 
				&& myHand.get(3).getType() == myHand.get(4).getType())
		{
			myRanking = PokerHandRanking.FULL_HOUSE;
			return true;
		}
		else if (check && myHand.get(1).getType() == myHand.get(2).getType() 
				&& myHand.get(2).getType() != myHand.get(3).getType() 
				&& myHand.get(3).getType() == myHand.get(4).getType())
		{
			myRanking = PokerHandRanking.FULL_HOUSE;
			return true;
		}
		else
		{
			return false;
		}
	}
	
	/**
	 * Method to check to see if the PokerHand is a Four of Kind.
	 * 
	 * @return true if the PokerHand is a Four of Kind, otherwise return false
	 * 
	 * @author Luisa Molina
	 * @author Erica Kok
	 */

	public boolean isFourOfKind()
	{
		/*
		 * Checks to see if the types of 4 Cards are the same.
		 */
		
		boolean matchTypes = false;
		for (int i=0; i < myHand.size() - 3; i++)
		{
			if (myHand.get(i).getType().getType() == myHand.get(i+1).getType().getType())
			{
				if (myHand.get(i+1).getType().getType() == myHand.get(i+2).getType().getType())
				{
					if (myHand.get(i+2).getType().getType() == myHand.get(i+3).getType().getType())
					{
						matchTypes = true;
					}
				}
			}
			else
			{
				matchTypes = false;
			}
		}
		
		/*
		 * If yes, then set myRanking to FOUR_OF_KIND and return true.
		 */

		if (matchTypes == true)
		{
			myRanking = PokerHandRanking.FOUR_OF_KIND;
			return true;
		}
		else
		{
			return false;
		}
	}
	
	/**
	 * Method to check to see if the PokerHand is a Straight Flush.
	 * 
	 * @return true if the PokerHand is a Straight Flush, otherwise return false
	 * 
	 * @author Luisa Molina
	 * @author Erica Kok
	 */

	public boolean isStraightFlush()
	{
		boolean matchSuits = true;
		boolean ascending = true;
		
		/*
		 * Checks to see if the suits of all the Cards are the same.
		 */

		for (int i = 0; i < myHand.size() - 1; i++)
		{
			if (myHand.get(i).getSuit().getSuit().equals(myHand.get(i + 1).getSuit().getSuit()))
			{
				matchSuits = matchSuits && true;
			} 
			else
			{
				matchSuits = false;
				break;
			}
		}
		
		/*
		 * If yes, next make sure that the types of the Cards are in ascending order.
		 */

		if (matchSuits == true)
		{
			for (int i = 0; i < myHand.size()-1; i++)
			{
				if (myHand.get(i).getType().getType()+1 == myHand.get(i+1).getType().getType())
				{
					ascending =  ascending && true;
				}
				else
				{
					ascending = false;
				}
			}
		}
		
		/*
		 * If both conditions above are met, set myRanking to STRAIGHT_FLUSH.
		 */

		if (matchSuits == true && ascending == true)
		{
			myRanking = PokerHandRanking.STRAIGHT_FLUSH;
			return true;
		}
		else
		{
			return false;
		}
	}
	
	/**
	 * Method to check to see if the PokerHand is a Royal Flush.
	 * 
	 * @return true if the PokerHand is a Royal Flush, otherwise return false
	 * 
	 * @author Luisa Molina
	 * @author Erica Kok
	 */

	public boolean isRoyalFlush()
	{
		boolean matchSuits = true;
		boolean myTypes = true;
		
		/*
		 * Checks to see if the suits of the Cards are all the same.
		 */
		
		for (int i = 0; i < myHand.size() - 1; i++)
		{
			if (myHand.get(i).getSuit().getSuit().equals(myHand.get(i + 1).getSuit().getSuit()))
			{
				matchSuits = matchSuits && true;
			}
			else
			{
				matchSuits = false;
				break;
			}
		}
		
		/*
		 * If yes, check to see if the types are 10, 11, 12, 13, 14.
		 */

		if(matchSuits == true)
		{
			for (int i = 0; i < myHand.size(); i++)
			{
				for (int j=10; j<15; j++)
				{
					if (myHand.get(i).getType().getType() == j)
					{
						myTypes = true;
					}
					else
					{
						myTypes = false;
					}
				}
			}
		}
		
		/*
		 * If both conditions above are met, set myRanking to ROYAL_FLUSH.
		 */

		if (myHand.size() == 5 && matchSuits == true && myTypes == true)
		{
			myRanking = PokerHandRanking.ROYAL_FLUSH;
			return true;
		}
		else
		{
			return false;
		}
	}
}
