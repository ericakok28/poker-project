package model;

/**
 * @purpose
 * 
 * Class for a hand within a poker game requiring an integer input
 * to determine the number of cards in a player's hand.
 * 
 * @author Luisa Molina
 * @author Erica Kok
 * 
 * @dueDate 3/21/16
 */

import java.util.Collections;
import java.util.Comparator;
import java.util.Vector;

public class Hand
{
	protected int myMaxNumberCards;
	protected Vector<Card> myHand;
	
	/**
	 * Constructor, creates a new Hand with a max number of 5 Cards.
	 * 
	 * @param max number of Cards
	 * 
	 * @author Luisa Molina
	 * @author Erica Kok
	 */

	public Hand(int maxNum)
	{
		myHand = new Vector<Card>(maxNum);
		myMaxNumberCards = maxNum;
	}

	/**
	 * Method to add a card to the hand and tests whether it is a duplicate.
	 * 
	 * @param card An object of type Card with values set in the constructor.
	 * 
	 * @return boolean on whether the operation occurred
	 * 
	 * @author Luisa Molina
	 * @author Erica Kok
	 */
	
	public boolean add(Card card)
	{
		boolean i = true;
		for(Card c : myHand)
		{
			CardType a1 = c.getType();
			CardSuit a2 = c.getSuit();
			i = !(card.getType() == a1 && card.getSuit() == a2);
			if(!i)
			{
				break;
			}
		}
		if(myMaxNumberCards != myHand.size())
		{
			if(i || myHand.isEmpty())
			{
				myHand.add(card);
				return true;
			}
		}
		return false;
	}

	/**
	 * Method to discard up to 3 cards in a hand if the vector is not empty.
	 * 
	 * @param indices Takes in a vector that has the index of the cards to be discarded.
	 * 
	 * @return returns a vector of the cards that were discarded.
	 * 
	 * @author Luisa Molina
	 * @author Erica Kok
	 */
	
	public Vector<Card> discard(Vector<Integer> indices)
	{
		Vector<Card> removed = new Vector<Card>();
		if(!indices.isEmpty() && indices.size() < 4)
		{
			Collections.sort(indices, Collections.reverseOrder());
			for(int i : indices)
			{
				if(i == myHand.size()-1)
				{
					removed.addElement(myHand.lastElement());
				}
				else
				{
					removed.addElement(myHand.get(i));
				}
				myHand.remove(i);
			}
		}
		return removed;
	}
	
//	
//	Vector<Card> discards = new Vector<Card>(0);
//	for(int j=0; j<myCards.size(); j++)
//	{
//		if(myCards.get(j).isSelected())
//		{
//			discards.add(myCards.remove(j));
//			j--;
//		}
//	}
//	return discards;
	

	/**
	 * Method to order the Cards in the hand by their card type from least to greatest.
	 * 
	 * @author Luisa Molina
	 * @author Erica Kok
	 */
	
	public void orderCards()
	{
		Collections.sort(myHand, new Comparator<Card>()
		{
			public int compare(Card o1, Card o2)
			{
				return o1.compareTo(o2);
			}
		});
	}
	
	/**
	 * Method to print the Cards in myHand as a String.
	 * 
	 * @return vector of Cards in myHand as a String
	 * 
	 * @author Luisa Molina
	 * @author Erica Kok
	 */

	public String toString()
	{
		return "Hand: " + myHand;
	}
	
	/**
	 * Method to get the number of Cards in the Hand.
	 * 
	 * @return number of Cards in Hand
	 * 
	 * @author Luisa Molina
	 * @author Erica Kok
	 */

	public int getNumberCardsInHand()
	{
		return myHand.size();
	}
	
	/**
	 * Method to get all the Cards in the Hand.
	 * 
	 * @return vector of Cards in myHand
	 * 
	 * @author Luisa Molina
	 * @author Erica Kok
	 */

	public Vector<Card> getCards()
	{
		return myHand;
	}
}
