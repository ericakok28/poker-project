package model;

/**
 * @purpose
 * 
 * Class used for what a player can and cannot do within a poker game.
 * 
 * @author Luisa Molina
 * @author Erica Kok
 * 
 * @dueDate 3/21/16
 */

public class Player
{
	private static final String DEFAULT_NAME = "JohnCena";
	protected String myName;
	protected int myNumberWins;
	protected boolean myAmAI;
	protected PokerHand myHand;

	public Player()
	{
		myName = DEFAULT_NAME;
		myAmAI = false;
		myHand = new PokerHand(5);
	}
	
	/**
	 * Constructor, creates a new Player with a name.
	 * 
	 * @param name
	 * 
	 * @author Luisa Molina
	 * @author Erica Kok
	 */

	public Player(String name)
	{
		myHand = new PokerHand(5);
		myAmAI = false;
		if (name == null)
		{
			myName = DEFAULT_NAME;
		}
		else if (validateName(name) == true)
		{
			myName = name;
		}
		else
		{
			myName = DEFAULT_NAME;
		}
	}
	
	/**
	 * Method to validate the name of the Player. A Player can only
	 * have a name with characters from A-Z or a-z, no special characters
	 * allowed. Otherwise, the Default Name JohnCena will be set for the
	 * Player.
	 * 
	 * @param name
	 * 
	 * @return true if name passed in is valid, otherwise return false
	 * and set the name to the default name "JohnCena"
	 * 
	 * @author Luisa Molina
	 * @author Erica Kok
	 */

	public boolean validateName(String name)
	{
		if (name.matches("^[A-Za-z]+$"))
		{
			return true;
		}
		else
		{
			name = DEFAULT_NAME;
			return false;
		}
	}
	
	/**
	 * Method to increment the number of wins of a Player.
	 * 
	 * @return the number of wins of a Player.
	 * 
	 * @author Luisa Molina
	 * @author Erica Kok
	 */

	public int incrementNumberWins()
	{
		myNumberWins += 1;
		return myNumberWins;
	}
	
	/**
	 * Method to print the Player's name as a String.
	 * 
	 * @return String of Player's name
	 * 
	 * @author Luisa Molina
	 * @author Erica Kok
	 */

	public String toString()
	{
		return myName;
	}
	
	/**
	 * Method to clone the Player.
	 * 
	 * @return the Object that's the clone of the Player
	 * 
	 * @author Luisa Molina
	 * @author Erica Kok
	 */

	public Object clone()
	{
		Object myClone = myName;
		return myClone;
	}
	
	/**
	 * Method to get the Hand of the Player.
	 * 
	 * @return the PokerHand of the Player.
	 * 
	 * @author Luisa Molina
	 * @author Erica Kok
	 */

	public PokerHand getHand()
	{
		return myHand;
	}
	
	/**
	 * Method to get the Player's name.
	 * 
	 * @return Player's name as a String
	 * 
	 * @author Luisa Molina
	 * @author Erica Kok
	 */

	public String getName()
	{
		return myName;
	}
	
	/**
	 * Method to get the Player's number of wins.
	 * 
	 * @return Player's number of wins
	 * 
	 * @author Luisa Molina
	 * @author Erica Kok
	 */

	public int getNumberWins()
	{
		return myNumberWins;
	}
	
	/**
	 * Method to check if the Player is an AI or not.
	 * 
	 * @return true if the Player is an AI, otherwise return false
	 * 
	 * @author Luisa Molina
	 * @author Erica Kok
	 */

	public boolean getAmAI()
	{
		return myAmAI;
	}
}
