package model;

/**
 * @purpose
 * 
 * Enumeration Class for the suits of each Card.
 *
 * @author Luisa Molina
 * @author Erica Kok
 * 
 * @dueDate 3/21/16
 */

public enum CardSuit
{
	SPADES ("Spades"),
	DIAMONDS ("Diamonds"),
	HEARTS ("Hearts"),
	CLUBS ("Clubs");

	private final String mySuit;
	
	/**
	 * Constructor, creates a new CardSuit and passing in the string
	 * name of the CardSuit.
	 * 
	 * @param suit name
	 * 
	 * @author Erica Kok
	 * @author Luisa Molina
	 */

	private CardSuit(String name) 
	{
		mySuit = name;
	}
	
	/**
	 * Method to get the suit of the Card.
	 * 
	 * @return suit of the Card as a string
	 * 
	 * @author Erica Kok
	 * @author Luisa Molina
	 */

	public String getSuit()
	{
		return mySuit;
	}


}
