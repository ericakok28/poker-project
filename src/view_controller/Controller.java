package view_controller;

/**
 * @purpose
 * 
 * Class that serves as the connection between the model and the view.
 * 
 * @author Erica Kok
 * 
 *@dueDate 3/31/16
 */

import java.awt.Color;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

import model.*;

public class Controller
{
	public View myView;
	public PokerModel myPokerModel;
	private Player myPlayer;

	int playerRanking, playerScore;
	int compRanking, compScore;
	
	Vector<model.Card> discard = new Vector<model.Card>(3);
	Vector<Integer> toDiscard = new Vector<Integer>(3);
	
	String playerName = JOptionPane.showInputDialog(null, "Enter a your name: ");
	ComputerPlayer myComp;

	/**
	 * Controller constructor; view must be passed in since controller has
	 * responsibility to notify view when some event takes place.
	 * 
	 * @param myView
	 * 
	 * @author Erica Kok
	 */

	public Controller()
	{
		myPlayer = new Player(playerName);
		myPokerModel = new PokerModel(myPlayer);
		myComp = (ComputerPlayer) myPokerModel.getPlayer(1);
		
		myView = new View(this);
	}
	
	/**
	 * Method to start the game.
	 * 
	 * @author Erica Kok
	 */

	public void startGame()
	{
		myPokerModel.resetGame();
		myPokerModel.dealCards();
		myPokerModel.getPlayer(0).getHand().orderCards();
		myPokerModel.getPlayer(1).getHand().orderCards();

		myView.getHandRanking("Your Hand is: "  + myPokerModel.getPlayer(0).getHand().determineRanking());
		
		for (int i = 0; i < 5; i++)
		{
			myPokerModel.getPlayer(0).getHand().getCards().get(i).flip();
			ImageIcon myNewImage = new ImageIcon(myPokerModel.getPlayer(0).getHand().getCards().get(i).getImage());
			myView.changePlayerImage(i, myNewImage);
		}
		
		myView.myEvents.setText(null);
		myView.myStartButton.setVisible(false);
		myView.myDiscardButton.setVisible(true);
	}
	
	/**
	 * Method to toggle the cards.
	 * 
	 * @param selected, the cards that are selected
	 * 
	 * @author Erica Kok
	 */

	public void toggleCards(Integer selected)
	{
		myPokerModel.getPlayer(0).getHand().getCards().get(selected).toggleSelected();
		
		for (int i = 0; i<5; i++)
		{
			boolean pressed = myPokerModel.getPlayer(0).getHand().getCards().get(i).isSelected();
			if (pressed == true)
			{
				myView.myPCards[i].setBorder(BorderFactory.createMatteBorder(3, 3, 3, 3, Color.RED));
			}
			else
			{
				myView.myPCards[i].setBorder(null);
			}
		}
		
		//myView.myPCards[selected].setBorder(BorderFactory.createMatteBorder(3, 3, 3, 3, Color.RED));
		
//		Card[] selectedCards = new Card[3];
//		
//		/*
//		 * Checks to see if only 3 cards were selected, toggles those 3 cards.
//		 */
//		
//		if(selectedCards.length <= 3)
//		{
//			myPokerModel.getPlayer(0).getHand().getCards().get(selected).toggleSelected();
//			for (int i = 0; i < 5; i++)
//			{
//				boolean pressed = myPokerModel.getPlayer(0).getHand().getCards().get(i).isSelected();	
		
//				if (pressed == true)
//				{
//					selectedCards[i] = myPokerModel.getPlayer(0).getHand().getCards().get(i);
//					myView.myPCards[i].setBorder(BorderFactory.createMatteBorder(3, 3, 3, 3, Color.RED));
//					
//				}
//				else
//				{
//					myView.myPCards[i].setBorder(null);
//				}
//			}
//		}
//		else
//		{
//			myView.myPCards[0].setEnabled(false);
//		}
//		
		System.out.println(selected);
		
		/*
		 * If yes, checks to see which of the 5 cards were selected
		 * then adds a border around those cards.
		 */
		
	}
	
	/**
	 * Method to discard the cards from the Hand of the Player and the 
	 * Hand of the ComputerPlayer. This also determines the winner of
	 * the round and asks the Player if he/she wants to play again.
	 * The game automatically ends if one player reaches the score of 3.
	 * 
	 * @author Erica Kok
	 */

	public void discard()
	{
		System.out.println(myPokerModel.getPlayer(0).getHand().getCards());
		
		for (int i = 0; i < 5; i++)
		{
			boolean isSelected = myPokerModel.getPlayer(0).getHand().getCards().get(i).isSelected();
			if (isSelected == true)
			{
				discard.add(myPokerModel.getPlayer(0).getHand().getCards().get(i));
				toDiscard.add(i);
			}
		}
		
		myPokerModel.getPlayer(0).getHand().discard(toDiscard);
		System.out.println(myPokerModel.getPlayer(0).getHand().getCards());
		
		for (int i = 0; i < toDiscard.size(); i++)
		{
			myView.myPCards[toDiscard.get(i)].setBorder(null);
			myPokerModel.dealCards();
		}
		
		myPokerModel.getPlayer(0).getHand().orderCards();
		
		for (int i = 0; i < 5; i++)
		{
			ImageIcon myNewImage = new ImageIcon(myPokerModel.getPlayer(0).getHand().getCards().get(i).getImage());
			myView.changePlayerImage(i, myNewImage);
		}
		
		System.out.println(myPokerModel.getPlayer(0).getHand().getCards());
		
		System.out.println("CompHand: " + myPokerModel.getPlayer(1).getHand().getCards());
		
		myComp.getHand().getCards().remove(myComp.selectCardsToDiscard());
		myPokerModel.dealCards();
		myComp.getHand().orderCards();
		
		System.out.println("NewCompHand: " + myPokerModel.getPlayer(1).getHand().getCards());
		
		myView.updateText("You discarded " + toDiscard.size() + " cards. Hal discarded " +  myComp.selectCardsToDiscard().size() + " cards.");
		
		Player winner = myPokerModel.determineWinner();
		
		int result = JOptionPane.showConfirmDialog(null, 
				   winner + " won this round! Play Again?", null, JOptionPane.YES_NO_OPTION);
				if (result == JOptionPane.NO_OPTION) {
				    System.exit(0);
				} 
				else if(result == JOptionPane.YES_OPTION)
				{
					myPokerModel.myRound++;
					myPokerModel.resetGame();
					startGame();
				}	
		
		System.out.println(winner);
		
		if (winner == myPokerModel.getPlayer(1))
		{
			myPokerModel.getPlayer(1).incrementNumberWins();
			myView.updateCScore("Hal's Score: " + myPokerModel.getPlayer(1).getNumberWins());
			if (myPokerModel.getPlayer(1).getNumberWins() == 3)
			{
				JOptionPane.showMessageDialog(null,
					    myPokerModel.getPlayer(1) + " won! Game Over.",
					    "Game Over",
					    JOptionPane.PLAIN_MESSAGE);
				myView.myFrame.setVisible(false);
				System.exit(0);
			}
		}
		
		else
		{
			myPokerModel.getPlayer(0).incrementNumberWins();
			myView.updatePScore(myPokerModel.getPlayer(0).getName() + "'s Score: "+myPokerModel.getPlayer(0).getNumberWins());
			if (myPokerModel.getPlayer(0).getNumberWins() == 3)
			{
				JOptionPane.showMessageDialog(null,
					    myPokerModel.getPlayer(0) + " won! Game Over.",
					    "Game Over",
					    JOptionPane.PLAIN_MESSAGE);
				myView.myFrame.setVisible(false);
				System.exit(0);
			}
		}
		
	
	}
}