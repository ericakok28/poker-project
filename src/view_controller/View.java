package view_controller;

/**
 * @purpose
 * 
 * Class that gives you the view of the Poker Game.
 * 
 * @author Erica Kok
 * 
 * @dueDate 3/31/16
 */

import java.awt.*;
import java.lang.reflect.*;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import model.Player;

@SuppressWarnings("serial")

public class View extends Frame
{

	private ButtonListener[] mySquareListener;
	private ButtonListener myStartListener, myDiscardListener;

	protected JFrame myFrame;
	protected JPanel myPanel, myCompPanel, myPlayerPanel, myNorthPanel;
	protected JLabel background, myPCards[], myCCards[], myPScore, myCScore, myEvents, myHandRanking;

	public JButton myStartButton, myDiscardButton;

	private Controller myController;
	private ImageIcon myCardsInHand;

	/**
	 * View constructor used to lay out the view
	 * 
	 * <pre>
	 * pre:  none
	 * post: the view is set up and initialized
	 * </pre>
	 */

	public View(Controller controller)
	{
		myFrame = new JFrame("Poker Game");
		
		myFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		myFrame.setSize(600, 600);
		myController = controller;
		mySquareListener = new ButtonListener[5];
		
		

		myCCards = new JLabel[5];
		myPCards = new JLabel[5];
		myPScore = new JLabel(myController.myPokerModel.getPlayer(0).getName() + "'s Score: " + controller.playerScore);
		myCScore = new JLabel(myController.myPokerModel.getPlayer(1).getName() + "'s Score: " + controller.compScore);
		myEvents = new JLabel();
		myHandRanking = new JLabel();
		

		
		myCompPanel = new JPanel();
		myCompPanel.setLayout(new GridLayout(1, 5, 8, 8));

		myPlayerPanel = new JPanel();
		myPlayerPanel.setLayout(new GridLayout(1, 5, 8, 8));

		myCardsInHand = new ImageIcon("src/cards/E.GIF");
		
		for (int i = 0; i < 5; i++)
		{
			myCCards[i] = new JLabel(myCardsInHand);
			myCompPanel.add(myCCards[i]);

			myPCards[i] = new JLabel(myCardsInHand);
			myPlayerPanel.add(myPCards[i]);
		}

		myNorthPanel = new JPanel();
		myNorthPanel.setLayout(new GridLayout(1, 2, 8, 8));
		myNorthPanel.add(myPScore);
		myNorthPanel.add(myCScore);

		myPanel = new JPanel();
		myPanel.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();

		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridy = 0;
		myPanel.add(myNorthPanel, c);

		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridy = 1;
		myPanel.add(myCompPanel, c);

		myStartButton = new JButton("Start Game");
		myDiscardButton = new JButton("Discard");
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridy = 2;
		myPanel.add(myStartButton, c);
		myPanel.add(myDiscardButton, c);
		myDiscardButton.setVisible(false);
		myStartButton.addMouseListener(myStartListener);
		myDiscardButton.addMouseListener(myDiscardListener);
		
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridy = 3;
		myPanel.add(myHandRanking, c);

		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridy = 4;
		myPanel.add(myPlayerPanel, c);

		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridy = 5;
		myPanel.add(myEvents, c);
		
		myPanel.setBackground(Color.cyan);

		myFrame.add(myPanel);
		this.associateListeners();
		myFrame.setVisible(true);
	}

	/**
	 * Associates each component's listener with the controller and the correct
	 * method to invoke when triggered.
	 *
	 * <pre>
	 * pre:  the controller class has be instantiated
	 * post: all listeners have been associated to the controller
	 *       and the method it must invoke
	 * </pre>
	 */
	public void associateListeners()
	{
		Class<? extends Controller> controllerClass;
		Method startGameMethod, discardMethod, toggleCardsMethod;
		Class<?>[] classArgs;

		controllerClass = myController.getClass();

		startGameMethod = null;
		discardMethod = null;
		toggleCardsMethod = null;
		
		classArgs = new Class[1];

		try
		{
			classArgs[0] = Class.forName("java.lang.Integer");
		} 
		catch (ClassNotFoundException e)
		{
			String error;
			error = e.toString();
			System.out.println(error);
		}

		try
		{
			startGameMethod = controllerClass.getMethod("startGame", (Class<?>[]) null);
			discardMethod = controllerClass.getMethod("discard", (Class<?>[]) null);
			toggleCardsMethod = controllerClass.getMethod("toggleCards", classArgs);
		} 
		catch (NoSuchMethodException exception)
		{
			String error;
			error = exception.toString();
			System.out.println(error);
		} 
		catch (SecurityException exception)
		{
			String error;

			error = exception.toString();
			System.out.println(error);
		}

		int i;
		Integer[] args;
		myStartListener = new ButtonListener(myController, startGameMethod, null);
		myDiscardListener = new ButtonListener(myController, discardMethod, null);
		
		myStartButton.addMouseListener(myStartListener);
		myDiscardButton.addMouseListener(myDiscardListener);

		for (i = 0; i < 5; i++)
		{
			args = new Integer[1];
			args[0] = new Integer(i);
			mySquareListener[i] = new ButtonListener(myController, toggleCardsMethod, args);
			myPCards[i].addMouseListener(mySquareListener[i]);
		}
	}

	public void changePlayerImage(int column, ImageIcon image)
	{
		myPCards[column].setIcon(image);
	}

	public void updateScore()
	{
		Player winner = myController.myPokerModel.determineWinner();
		if (winner == myController.myPokerModel.getPlayer(0))
		{
			myController.playerScore++;
		}
		else
		{
			myController.compScore++;
		}
	}
	
	public void getHandRanking(String ranking)
	{
		myHandRanking.setText(ranking);
	}
	
	public void updateText(String text)
	{	
		myEvents.setText(text);
	}
	
	public void updatePScore(String score)
	{
		myPScore.setText(score);
	}
	
	public void updateCScore(String score)
	{
		myCScore.setText(score);
	}
}