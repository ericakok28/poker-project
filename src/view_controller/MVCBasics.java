package view_controller;

/**
 * @purpose
 * 
 * Class with the main that you run.
 * 
 * @author Daniel Plante
 * 
 * @dueDate 3/31/16
 */

public class MVCBasics
{
    // Properties
    private Controller myController;
    
    // Methods
    public static void main(String[] args)
    {
        new MVCBasics();
    }
    
    public MVCBasics()
    {
        setController(new Controller());
    }

	public void setController(Controller controller) 
	{
		myController = controller;
	}

	public Controller getController() {
		return myController;
	}
}